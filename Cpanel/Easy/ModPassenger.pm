package Cpanel::Easy::ModPassenger;

our $easyconfig = {
    'name'     => 'Mod Passenger',
    'url'      => 'https://www.phusionpassenger.com/',
    'note'     => 'mod_passenger for Apache 2.x',
    'hastargz' => 0,
    'src_cd2'  => '',
    'modself'  => sub {},
    'when_i_am_off' => sub {
        my ($self) = @_;
        # whatever, warn if something needs done, return not used since it's next()ing it anyway)
    },
    'step' => {
        '0' => {
            'name'    => 'Verify ruby install',
            'command' => sub {
                my ($self) = @_;
                if( ! -e '/usr/bin/ruby' )
                {
					$self->run_system_cmd( [ '/scripts/installruby' ], 0, 1 );
                } else {
					$self->print_alert(q{Ruby is also installed});
				}
            },
        },
		'1' => {
			'name'		=> 'Verify passenger gem',
			'command'	=> sub {
				my ($self) = @_;
				my $exists = 0;
				
				my @lines = qx(gem list passenger);
				foreach( @lines )
				{
					chomp;
					if( $_ =~ /passenger/ )
					{
						$exists = 1;
					}
				}
				if($exists == 0)
				{
					$self->run_system_cmd( [ 'gem install passenger' ], 0, 1 );
				} else {
					$self->print_alert(q{gem passenger also installed});
				}
			},
        },
        '2' => {
            'name'    => 'Install apache2 passenger module',
            'command' => sub {
                my ($self) = @_;
				($rc, @msg) = $self->run_system_cmd_returnable([ 
					qw(/usr/bin/passenger-install-apache2-module),
					qw(--auto),
					qw(--apxs2-path /usr/local/apache/bin/apxs),
					qw(--apr-config-path /usr/local/apache/bin/apr-1-config)
				]);

                return ($rc, @msg);
            },
        },
		'3' => {
			'name'	=> 'Apache config',
			'command' => sub {
				my ($self) = @_;

				my @apache_conf;
				my $line_jump = 0;
				my $data = qx(/usr/bin/passenger-install-apache2-module --snippet); chomp $data;
				
				# Beware previous config
				open( AP_CONF, "</usr/local/apache/conf/includes/pre_main_2.conf") or die "[ERROR]: Cant read pre_main_2.conf!\n";
				while( <AP_CONF> )
				{
					chomp;
					$line_jump = 1 if( $_ =~ m|#mod_passenger|  );
					$line_jump = 0 if( $_ =~ m|#/mod_passenger| );
					next if ( $line_jump == 1 );
					push( @apache_conf, $_ );
				}
				close( AP_CONF );
			
				open( AP_CONF, ">/usr/local/apache/conf/includes/pre_main_2.conf" ) or die "[ERROR]: Cant write pre_main_2.conf!\n";
				foreach( @apache_conf )
				{
					print AP_CONF "$_\n";
				}
				print AP_CONF "#mod_passenger\n";
				print AP_CONF "$data\n";
				print AP_CONF "#mod_passenger\n";
				close( AP_CONF );

			},
		},
    },
};

1;
